import bs4 as bs4
import requests


class VkBot:

    def __init__(self, user_id):
        print("\nСоздан объект бота!")

        self._USER_ID = user_id
        self._USERNAME = self._get_user_name_from_vk_id(user_id)

        self._COMMANDS = ["ПРИВЕТ", "ТАБЛИЦА", "ТРАНСЛЯЦИЯ"]
       
    # self._NAME = switcher.get(self._USER_ID)


    def _get_user_name_from_vk_id(self, user_id):
        request = requests.get("https://vk.com/id"+str(user_id))
        bs = bs4.BeautifulSoup(request.text, "html.parser")

        user_name = self._clean_all_tag_from_str(bs.findAll("title")[0])

        return user_name.split()[0]

    def new_message(self, message):

        # Привет
        if message.upper() == self._COMMANDS[0]:
            return """
            Доступные команды:
            Напишите "Таблица" - для получения ссылки на таблицу с результатами
            Напишите "Трансляция" - для получения ссылки на youtube трансляцию
            Всё остальное, кроме "Привет", расценивается как ответ на вопрос.
            Для того чтобы сдать ответ, нужно написать боту "Номер_Вопроса Ответ"
            Например:8 Пушкин
            """

        elif message.upper() == self._COMMANDS[1]:
            return "https://docs.google.com/spreadsheets/d/1Dvu5MXxQmK_yKQKg0uAwlGb4TVJBXMaTnL0hIHiyJ3Q/edit?usp=sharing"

        elif message.upper() == self._COMMANDS[2]:
            return "https://www.youtube.com/watch?v=3HGroxWRDaM&feature=youtu.be"
        else:
            return "Ответ принят"

    def _get_time(self):
        request = requests.get("https://my-calend.ru/date-and-time-today")
        b = bs4.BeautifulSoup(request.text, "html.parser")
        return self._clean_all_tag_from_str(str(b.select(".page")[0].findAll("h2")[1])).split()[1]

    @staticmethod
    def _clean_all_tag_from_str(string_line):

        """
        Очистка строки stringLine от тэгов и их содержимых
        :param string_line: Очищаемая строка
        :return: очищенная строка
        """

        result = ""
        not_skip = True
        for i in list(string_line):
            if not_skip:
                if i == "<":
                    not_skip = False
                else:
                    result += i
            else:
                if i == ">":
                    not_skip = True

        return result

